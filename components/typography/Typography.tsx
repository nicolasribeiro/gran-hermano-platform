import React, { ReactElement } from "react";

interface TypoProps {
  element: "p" | "span" | "h1" | "h2" | "h3" | "h4" | "h5" | "h6";
  className?: string;
  text: ReactElement | string | string[];
}

export const Typography = ({ element, text, className }: TypoProps) => {
  const JsxElement: { [key: string]: JSX.Element } = {
    p: <p className={className}>{text}</p>,
    span: <span className={className}>{text}</span>,
    h1: <h1 className={className}>{text}</h1>,
    h2: <h2 className={className}>{text}</h2>,
    h3: <h3 className={className}>{text}</h3>,
    h4: <h4 className={className}>{text}</h4>,
    h5: <h5 className={className}>{text}</h5>,
    h6: <h6 className={className}>{text}</h6>,
  };

  return JsxElement[element];
};
