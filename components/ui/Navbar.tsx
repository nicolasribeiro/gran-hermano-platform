import React from "react";
import { MenuIcon } from "@/components/icons/MenuIcon";
import { Typography } from "../typography/Typography";
import Link from "next/link";

export const Navbar = () => {
  return (
    <>
      <div className="navbar bg-primary">
        <div className="navbar-start">
          <div className="flex-none">
            <button className="btn btn-square btn-ghost">
              <MenuIcon />
            </button>
          </div>
          <div className="flex-1">
            <Typography
              element="h1"
              className="text-white text-xl font-bold"
              text={`Gran Hermano ${new Date().getFullYear().toString()}`}
            />
          </div>
        </div>
        <div className="navbar-center">
          <div className="flex flex-row items-center gap-8 uppercase">
            <Link
              className="transition-bg hover:border-b-2 border-spacing-1 border-accent"
              href="#/"
            >
              Principal
            </Link>
            <Link
              className="transition-bg hover:border-b-2 border-spacing-1 border-accent"
              href="#/"
            >
              Placa
            </Link>
            <Link
              className="transition-bg hover:border-b-2 border-spacing-1 border-accent"
              href="#/"
            >
              Participantes
            </Link>
          </div>
        </div>
        <div className="navbar-end" />
      </div>
    </>
  );
};
