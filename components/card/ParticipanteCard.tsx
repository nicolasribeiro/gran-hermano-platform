// ParticipanteCard.tsx

import { IParticipantes } from "@/interfaces/participantes.interface";
import Image from "next/image";
import React from "react";
import { capitalize } from "@/utils/formatters";
import Link from "next/link";

type ICard = {
  participante: IParticipantes;
};

export const ParticipanteCard = ({ participante }: ICard) => {
  const cardStyles = [
    "relative",
    "overflow-hidden",
    "w-300",
    "h-200",
    "transition-transform",
    "transform-gpu",
    participante.eliminado ? "grayscale" : null,
    "hover:scale-110",
  ].join(" ");

  return (
    <div className={cardStyles}>
      {participante.eliminado && (
        <div className="absolute top-0 left-0 w-full h-full bg-black bg-opacity-40 flex flex-col items-center justify-center text-white font-bold text-xl">
          ELIMINADO
        </div>
      )}
      {participante.nominado && (
        <div className="absolute top-0 right-0 bg-red-500 text-white p-2">
          Nominado
        </div>
      )}
      <Image
        className="w-full h-full object-cover"
        src={participante.img}
        alt={participante.nombre}
        width={740}
        height={620}
      />
      <div className="absolute top-0 left-0 w-full h-full bg-black bg-opacity-50 flex flex-col items-center justify-center text-white font-bold text-lg opacity-0 hover:opacity-100">
        <Link
          href="#/"
          className="text-white no-underline py-2 px-4 border-2 border-white rounded font-bold transition-bg bg-transparent"
        >
          {capitalize(participante.nombre)}
        </Link>
      </div>
    </div>
  );
};
