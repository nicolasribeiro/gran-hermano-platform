import React, { ReactNode } from "react";
import { Navbar } from "@/components/ui/Navbar";

interface LayoutProps {
  children?: ReactNode;
}

export const MainLayout = ({ children }: LayoutProps) => {
  return (
    <>
      <Navbar />
      <main className="flex flex-row">
        <div className="p-4">{children}</div>
      </main>
    </>
  );
};
