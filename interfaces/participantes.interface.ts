export interface IVotos {
  primero: string;
  segundo: string;
}

export interface IParticipantes {
  nombre: string;
  img: string;
  description?: string;
  nominado?: boolean;
  eliminado?: boolean;
  votos?: IVotos;
  espontanea?: boolean;
  fulminante?: boolean;
}
