import type { Metadata } from "next";
import "./globals.css";
import { MainLayout } from "@/components/layouts/MainLayout";

export const metadata: Metadata = {
  title: "Foro Gran Hermano",
  description: "Toda la informacion de Gran Hermano en un solo lugar",
  authors: [{ name: "Nicolas Ribeiro", url: "https://www.nicolasribeiro.com" }],
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="es">
      <body>
        <MainLayout>{children}</MainLayout>
      </body>
    </html>
  );
}
