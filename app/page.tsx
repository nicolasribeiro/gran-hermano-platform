import { ParticipanteCard } from "@/components/card/ParticipanteCard";
import { Typography } from "@/components/typography/Typography";
import { participantes } from "@/data/participantes";
import { NextPage } from "next";

const Home: NextPage = () => {
  return (
    <section>
      <div className="flex flex-row justify-center items-center mb-4">
        <Typography
          className="text-xl font-bold"
          element="h1"
          text={`Participantes Gran Hermano Argentina ${new Date().getFullYear()} `}
        />
      </div>
      <div className="grid grid-cols-6 gap-4">
        {participantes.map((participante, i) => (
          <ParticipanteCard key={i * Math.PI} participante={participante} />
        ))}
      </div>
    </section>
  );
};

export default Home;
