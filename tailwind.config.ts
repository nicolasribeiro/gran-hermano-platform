import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: "#7600fc",
          secondary: "#f6d860",
          accent: "#2a8dff",
          neutral: "#3d4451",
          "base-100": "#ffffff",
        },
      },
    ],
  },
};
export default config;
